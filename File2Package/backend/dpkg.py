import typing
from pathlib import Path
import sh
import json
from AnyVer import AnyVer
from PackageRef import VersionedPackageRef
from debparse.deb_control import parse as debParse

def getAptVars(*vars):
	aptCfgCommand = sh.Command("apt-config")
	rawRes = aptCfgCommand.dump(*vars)
	res = {}
	for l in rawRes.splitlines():
		if l[-1] == "\n":
			l = l[:-1]
		if l[-1] == ";":
			l = l[:-1]
		k, v = l.split(" ")
		res[k] = json.loads(v)
	return res


architectureRemap = {
	('Linux', 'x86_64'): "amd64",
	('Linux', 'x86'): "i386",
	('Linux', 'riscv64'): "riscv64",
	('GNU/Linux', 'x86_64'): "amd64",
	('GNU/Linux', 'x86'): "i386",
	('GNU/Linux', 'riscv64'): "riscv64",
	
	('GNU/kFreeBSD', 'x86_64'): "kfreebsd-amd64",
	('GNU/kFreeBSD', 'x86'): "kfreebsd-i386",
}

def getAptInfo():
	try:
		aptVars = getAptVars("Dir::State::status", "APT::Architecture")
		statusFile = Path(aptVars["Dir::State::status"])
		return statusFile, statusFile.parent / "info", aptVars["APT::Architecture"]
	except:
		import platform
		un = platform.uname()
		statusFileDir = Path("/var/lib/dpkg/")
		return statusFileDir / "status", statusFileDir / "info", architectureRemap[(un.system, un.machine)]

statusFileLoc, infoFilesLoc, defaultArch = getAptInfo()

def init(interfaces):
	class DPKGFile2PackagePopulator(interfaces.IFile2PackagePopulator):
		__slots__ = ("infoFilesLoc", "defaultArch")
		def __init__(self, infoFilesLoc:Path=infoFilesLoc, defaultArch:str=defaultArch):
			self.infoFilesLoc = infoFilesLoc
			self.defaultArch = defaultArch
		
		def packagesMappingYielder(self, pkgs):
			index = {pkg['Package'].text: AnyVer(pkg['Version'].text) for pkg in debParse(statusFileLoc).packages if 'Version' in pkg}
			
			for p in pkgs:
				nameArch = p.stem.split(":")
				if len(nameArch) != 1:
					name, arch = nameArch
				else:
					name = nameArch[0]
					arch = self.defaultArch
				
				if name in index:
					ref = interfaces.VersionedPackageRef(name, arch, version=index[name])
				else:
					ref = interfaces.BasePackageRef(name, arch)
				
				yield interfaces.FilesPackageMapping(ref, self.__class__.filesYielder(p))
		
		def __call__(self):
			pkgs = tuple(self.infoFilesLoc.glob("*.list"))
			return len(pkgs), self.packagesMappingYielder(pkgs)
		
		@staticmethod
		def filesYielder(p: Path):
			with p.open("rt", encoding="utf-8") as f:
				for l in f:
					yield Path(l.strip())

	return DPKGFile2PackagePopulator

